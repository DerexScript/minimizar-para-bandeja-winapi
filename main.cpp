#include <tchar.h>
#include <windows.h>
#include "resource.h"

using namespace std;
NOTIFYICONDATA nid = {};

void ShowContextMenu(HWND hWnd)
{
    POINT pt;
    GetCursorPos(&pt);
    HMENU hMenu = CreatePopupMenu();
    if(hMenu)
    {
        if( IsWindowVisible(hWnd) )
            InsertMenu(hMenu, -1, MF_BYPOSITION, APP_HIDE, _T("Hide"));
        else
            InsertMenu(hMenu, -1, MF_BYPOSITION, APP_SHOW, _T("Show"));
        InsertMenu(hMenu, -1, MF_BYPOSITION, APP_EXIT, _T("Exit"));


        SetForegroundWindow(hWnd);
        TrackPopupMenu(hMenu, TPM_BOTTOMALIGN,
                       pt.x, pt.y, 0, hWnd, NULL );
        DestroyMenu(hMenu);
    }
}

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam){
    switch (message){
        case APPWM_ICONNOTIFY:
            switch (lParam){
                case WM_LBUTTONUP:
                    if( IsWindowVisible(hwnd) )
                        ShowWindow(hwnd, SW_HIDE);
                    else
                        ShowWindow(hwnd, SW_RESTORE);
                    break;
                case WM_RBUTTONUP:
                    ShowContextMenu(hwnd);
                    break;
                case WM_CONTEXTMENU:
                    ShowContextMenu(hwnd);
                    break;
            }
            break;
        case WM_SYSCOMMAND:
            if((wParam & 0xFFF0) == SC_MINIMIZE)
            {
                ShowWindow(hwnd, SW_HIDE);
                return 1;
            }
            break;
        case WM_COMMAND:
            switch (LOWORD(wParam)){
                case APP_HIDE:
                    case IDOK:
                        ShowWindow(hwnd, SW_HIDE);
                        break;
                    break;
                case APP_SHOW:
                    ShowWindow(hwnd, SW_RESTORE);
                    break;
                case APP_EXIT:
                    DestroyWindow(hwnd);
                    break;
            }
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            nid.uFlags = 0;
            Shell_NotifyIcon(NIM_DELETE, &nid);
            PostQuitMessage(0);
            break;

    }
    return DefWindowProc (hwnd, message, wParam, lParam);
}

int WINAPI WinMain (HINSTANCE hThisInstance,HINSTANCE hPrevInstance,LPSTR lpszArgument,int nCmdShow){
    TCHAR szClassName[] = _T("AppToTray");
    HWND hwnd;
    MSG messages;
    WNDCLASSEX wincl;
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;
    wincl.style = CS_DBLCLKS;
    wincl.cbSize = sizeof (WNDCLASSEX);
    wincl.hIcon = LoadIcon(hThisInstance, MAKEINTRESOURCE(MYGAMES_ICON));
    wincl.hIconSm = LoadIcon(hThisInstance, MAKEINTRESOURCE(MYGAMES_ICON));
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;
    wincl.cbClsExtra = 0;
    wincl.cbWndExtra = 0;
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    if (!RegisterClassEx(&wincl))
        return 0;

    hwnd = CreateWindowEx (
               0,
               szClassName,
               _T("Example: Minimize to tray"),
               WS_OVERLAPPEDWINDOW,
               CW_USEDEFAULT,
               CW_USEDEFAULT,
               544,
               375,
               HWND_DESKTOP,
               NULL,
               hThisInstance,
               NULL
           );
    ShowWindow (hwnd, nCmdShow);

    //Ocultar janela
    ShowWindow(hwnd, SW_HIDE);

    //preenchendo strtura para fun��o Shell_NotifyIconA
    HICON hIcon = static_cast<HICON>(LoadImage(NULL,
                                     TEXT("img\\logo.ico"),
                                     IMAGE_ICON,
                                     0, 0,
                                     LR_DEFAULTCOLOR | LR_SHARED | LR_DEFAULTSIZE | LR_LOADFROMFILE));
    nid.cbSize = sizeof(nid);
    nid.hWnd = hwnd;
    nid.uID = 1;
    nid.uFlags = NIF_ICON|NIF_TIP|NIF_MESSAGE;
    nid.uCallbackMessage = APPWM_ICONNOTIFY;
    nid.hIcon = hIcon;
    strcpy(nid.szTip, "Games");

    //fun��o que cria o icone na bandeja do windows!
    Shell_NotifyIconA(NIM_ADD, &nid);

    while (GetMessage (&messages, NULL, 0, 0)){
        TranslateMessage(&messages);
        DispatchMessage(&messages);
    }
    return messages.wParam;
}
